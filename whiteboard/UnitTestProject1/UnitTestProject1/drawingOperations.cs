﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using System.Web;
using System.IO;
using System.Linq;
using OpenQA.Selenium.Support.UI;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows.Input;
using System.Drawing;
using System.Threading;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Interactions.Internal;

namespace UnitTestProject1
{
    [TestClass]
    public class drawingOperations
    {
        static IWebDriver driver;

        [TestInitialize()]
        public void setUp()
        {
            driver = new ChromeDriver();
            driver.Manage().Window.Maximize();
            driver.Url = "https://e3bcd7c9-dd9c-4792-86a1-285c5e4ca398.tsfx.com.au/#/WhiteBoard";
            Thread.Sleep(10000);

        }
        [TestMethod]
        public void openDocument()
        {
            driver.FindElement(By.ClassName("add-new-doc")).Click();
            Thread.Sleep(5000);
            driver.FindElement(By.ClassName("new-doc-page-e")).Click();
            Thread.Sleep(10000);
            driver.FindElement(By.XPath(".//div[@class='modal-content']/div/div[@class='modal-footer']/button[contains(@class,'btn btn-primary')]")).Click();
        }

        [TestMethod]
        public void drawLinePen()
        {
            var selectPenTool = driver.FindElement(By.ClassName("tool-pen"));
            selectPenTool.Click();
            Thread.Sleep(1000);
            var drawingDocument = driver.FindElement(By.Id("doc196-page1-0"));
            OpenQA.Selenium.Interactions.Actions builder = new OpenQA.Selenium.Interactions.Actions(driver);

            try
            {
                //builder.DragAndDrop(selectPenTool, drawingDocument).Build().Perform();
                builder.MoveToElement(selectPenTool).Perform();
                Thread.Sleep(3000);
                builder.DragAndDropToOffset(drawingDocument, 100, 150).Build().Perform();
                Thread.Sleep(5000);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

        }
        [TestMethod]
        public void drawLinePenWithAllColor()
        {
            var selectPenTool = driver.FindElement(By.ClassName("tool-pen"));
            //selectPenTool.Click();
            Thread.Sleep(2000);
            //Actions builder = new Actions();
            var firstPenLineOption = driver.FindElement(By.ClassName("stroke wbicon-line-1"));
            OpenQA.Selenium.Interactions.Actions builder = new OpenQA.Selenium.Interactions.Actions(driver);
            var drawingDocument = driver.FindElement(By.Id("doc196-page1-0"));
            //Actions action = new Actions(driver);
            builder.ClickAndHold(selectPenTool).Perform();
            Thread.Sleep(5000);
            builder.MoveToElement(firstPenLineOption).Perform();
            Thread.Sleep(5000);
            builder.Click().Build().Perform();
            Thread.Sleep(5000);
            builder.MoveToElement(drawingDocument, 100, 100).Perform();
            Thread.Sleep(5000);
            builder.Perform();

            //builder.KeyDown(selectPenTool, Keys.Control).Perform();
            //Thread.Sleep(5000);
            //builder.MoveToElement(firstPenLineOption).Perform();
            //Thread.Sleep(5000);
            //builder.KeyUp(firstPenLineOption,Keys.Control).Perform();
            //Thread.Sleep(2000);
            //var drawingDocument = driver.FindElement(By.Id("doc196-page1-0"));
            //try
            //{

            //    //builder.DragAndDropToOffset(drawingDocument, 100, 100).Build().Perform();
            //    Thread.Sleep(5000);
            //}
            //catch (Exception e)
            //{
            //    Console.WriteLine(e.Message);
            //}

        }
        [TestCleanup]
        public void closeBrowser()
        {
            driver.Quit();
        }
    }
}
