﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System.Net;
using OpenQA.Selenium.Firefox;
using System.Web;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows.Input;
using System.Drawing;
using System.Threading;
using System.Xml.Linq;
using System.Xml;
using System.Windows.Forms;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Interactions.Internal;

namespace UnitTestProject1.functions
{
    [TestClass]
    public class topBarFunctions
    {
        static IWebDriver driver;

        public topBarFunctions(IWebDriver iWebDriver)
        {
            driver = iWebDriver;
        }
        
        public void deleteAllOpenedDocument()
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(999999999));

            //following will count total opened document by counting "li" tag
            int countLiTags = 0;
            countLiTags = driver.FindElements(By.XPath(".//div[@class='whiteboard-documents ng-isolate-scope all-documents-loaded-e']/ul/li")).Count();
            
            for (int i = 0; i < countLiTags - 2; i++)
            {
                //following will click on navicon icon button of each opened document one by one
                driver.FindElements(By.XPath(".//div[@class='navicon document-header-navicon-e ng-scope']")).ElementAt(0).Click();
                Thread.Sleep(2000);
                //wait.Until(driver => driver.FindElement(By.XPath(".//div[@class='navicon document-header-navicon-e ng-scope']/div[@class='multi-dropdown open']/ul[@class='dropdown-menu']/li")));

                //following will click on "close" button from opened menu by clicking on navicon icon button
                driver.FindElements(By.XPath(".//div[@class='navicon document-header-navicon-e ng-scope']/div[@class='multi-dropdown open']/ul[@class='dropdown-menu']/li")).Last().Click();
            }
        }
        
        public void openNewDocument()
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(999999999));

            //following will fetch "+" button in order to open new document
            driver.FindElement(By.ClassName("add-new-doc")).Click();
            //Thread.Sleep(6000);
            wait.Until(driver => driver.FindElement(By.ClassName("new-doc-page-e")));

            //following will fetch "New" button in order to open new document
            driver.FindElement(By.ClassName("new-doc-page-e")).Click();
            //Thread.Sleep(6000);
            wait.Until(driver => driver.FindElement(By.XPath(".//div[@class='modal-dialog']/div[@class='modal-content']/div[@class='document-availability ng-scope']/div[@class='modal-footer']/button[@class='btn btn-primary']")));

            //following line will fetch "OK" button element from opened window in order to open new document
            driver.FindElement(By.XPath(".//div[@class='modal-dialog']/div[@class='modal-content']/div[@class='document-availability ng-scope']/div[@class='modal-footer']/button[@class='btn btn-primary']")).Click();
            //Thread.Sleep(6000);
        }

        public void openEndLessDocument()
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(999999999));
            //following will fetch "+" button in order to open new document
            driver.FindElement(By.ClassName("add-new-doc")).Click();
            //Thread.Sleep(6000);
            wait.Until(driver => driver.FindElement(By.ClassName("new-doc-page-e")));

            //following will fetch "New" button in order to open new document
            driver.FindElement(By.ClassName("endless-mode-tool-e")).Click();
            //Thread.Sleep(6000);
            wait.Until(driver => driver.FindElement(By.XPath(".//div[@class='modal-dialog']/div[@class='modal-content']/div[@class='document-availability ng-scope']/div[@class='modal-footer']/button[@class='btn btn-primary']")));

            //following line will fetch "OK" button element from opened window in order to open new document
            driver.FindElement(By.XPath(".//div[@class='modal-dialog']/div[@class='modal-content']/div[@class='document-availability ng-scope']/div[@class='modal-footer']/button[@class='btn btn-primary']")).Click();
        }

        public void renameDocument()
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(999999999));
            //following will wait for "Document Name" element
            wait.Until(driver => driver.FindElement(By.XPath(".//div[@class='whiteboard-documents ng-isolate-scope all-documents-loaded-e']/ul/li[@class='document-tab-header whiteboard-document-tab doc-type-0-e  active']/a/tab-heading/div[@class='heading-content']/div")));

            //following will double click on "Document Name" in order to change it's name
            Actions actions = new Actions(driver);
            actions.MoveToElement(driver.FindElement(By.XPath(".//div[@class='whiteboard-documents ng-isolate-scope all-documents-loaded-e']/ul/li[@class='document-tab-header whiteboard-document-tab doc-type-0-e  active']/a/tab-heading/div[@class='heading-content']/div"))).DoubleClick().Build().Perform();
            Thread.Sleep(5000);
            actions.MoveToElement(driver.FindElement(By.XPath(".//div[contains(@class,'whiteboard-documents')]/ul/li[contains(@class,'document-tab-header')and contains(@class,'active')]/a/tab-heading/div[@class='heading-content']/form[contains(@class,'note-form')]/input[1]"))).SendKeys("Test").Perform();
        }
    }
}
