﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System.Net;
using OpenQA.Selenium.Firefox;
using System.Web;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows.Input;
using System.Drawing;
using System.Threading;
using System.Xml.Linq;
using System.Xml;
using System.Windows.Forms;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Interactions.Internal;
using UnitTestProject1.functions;

namespace UnitTestProject1
{
    [TestClass]
    public class addDocument
    {
        static IWebDriver driver;
        topBarFunctions topBarFunctions;
        [TestInitialize]
        public void setup()
        {
            driver = new ChromeDriver();            
            driver.Url = "https://e6112650-3adf-494f-895b-4fdea2b9c885.tsfx.com.au/#/WhiteBoard";
            driver.Manage().Window.Maximize();            
            Thread.Sleep(10000);
        }

        [TestMethod]
        public void renameDocument()
        {
            topBarFunctions = new topBarFunctions(driver);
            topBarFunctions.deleteAllOpenedDocument();
            topBarFunctions.openNewDocument();
            topBarFunctions.renameDocument();
        }

        [TestCleanup]
        public void cleanUp()
        {
            driver.Close();
        }
    }
}
