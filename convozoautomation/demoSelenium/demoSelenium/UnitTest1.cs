﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using System.Web;
using System.IO;
using System.Linq;
using OpenQA.Selenium.Support.UI;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows.Input;
using System.Drawing;
using System.Threading;



namespace demoSelenium
{
    [TestClass]
    public class UnitTest1
    {
        static IWebDriver driver;

        [TestInitialize()]
        public void setUp()
        {
            driver = new FirefoxDriver();
            driver.Url = "http://www.google.co.in";
            Thread.Sleep(3000);
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            wait.Until(driver => driver.FindElement(By.Id("lst-ib")));

        }

        [TestMethod]
        public void TestFireFoxDriver1()
        {
            //driverFF.Navigate().GoToUrl("http://www.google.com");
            driver.FindElement(By.Id("lst-ib")).SendKeys("Selenium");
            Thread.Sleep(3000);
            driver.FindElement(By.Id("lst-ib")).SendKeys(Keys.Enter);
        }
  

        [TestMethod]
        public void TestFireFoxDriver2()
        {
            //driverGC.Navigate().GoToUrl("http://www.google.com");
            driver.FindElement(By.Id("lst-ib")).SendKeys("Protractor");
            Thread.Sleep(3000);
            driver.FindElement(By.Id("lst-ib")).SendKeys(Keys.Enter);
        }
 

        [TestMethod]

        public void TestChromeForDriver1()
        {
            driver.FindElement(By.Id("lst-ib")).SendKeys("Protractor");
            Thread.Sleep(3000);
            driver.FindElement(By.Id("lst-ib")).SendKeys(Keys.Enter);
        }

        [TestCleanup]
        public void closeBrowser()
        {
            driver.Quit();
        }

    }
}