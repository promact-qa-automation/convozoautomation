Given(/^The App has Launched$/) do
#pending
end

#This scenario is used to click on 'Skip' button first time while convozo is launch first time on device 
Then(/^I should see Skip$/) do
	wait_poll(:until_exists => "* text:'Skip'", :timeout => 20) do
	end
	puts element_exists("* text:'Skip'")
end

Then(/^I touch Skip$/) do
	wait_poll(:until_exists => "* text:'Skip'", :timeout => 20) do
	end
	skipElement = query("* text:'Skip'")
	touch(skipElement)
end

Then(/^I should see Login Page$/) do
	wait_poll(:until_exists => "* text:'Sign in with twitter'", :timeout => 20) do
	end
	puts element_exists("* text:'Sign in with twitter'")
	
end

#This scenario is used to find 'UPVOTE' or 'UPVOTED' button on available page of application and click on it
Then(/^I should see Upvote or Upvoted button$/) do
	wait_poll(:until_exists => "* marked:'textView_Wall_liked'", :timeout => 20) do
	end
	puts element_exists("* marked:'textView_Wall_liked'")
end

textBeforeClick = nil
Then(/^I touch Upvote or Upvoted button$/)do
    wait_poll(:until_exists => "* marked:'textView_Wall_liked'", :timeout => 30) do
	end
	elementChkText = query("* marked:'textView_Wall_liked'")
	textBeforeClick = query("* marked:'textView_Wall_liked'", :text).first
	touch(elementChkText)
end

Then(/^I should see Upvoted or Upvote button$/)do
	wait_poll(:until_exists => "* marked:'textView_Wall_liked'", :timeout => 20) do
	
	end
	if textBeforeClick == "UPVOTE"
		buttonName = "UPVOTED"
		textAfterClick = query("* marked:'textView_Wall_liked'", :text).first
		puts "Now button text is upvoted: #{(textAfterClick).eql? (buttonName)}"
	else
		buttonName = "UPVOTE"
		textAfterClick = query("* marked:'textView_Wall_liked'", :text).first
		puts "Now button text is upvote: #{(textAfterClick).eql? (buttonName)}"
	end
end

#This scenario is used to find 'UPVOTE' button in etire page of application using scroll down functionality and if 'UPVOTE' is find then click on it
isExist = false
Then (/^I touch Upvote button$/)do
	
	wait_poll(:until_exists => "* text:'UPVOTE'", :timeout => 20) do
		scroll("RecyclerView", :down)
		
		if element_exists("* marked:'load_more_progress_bar'")
			isExist = true
			break
		end
	end
	if isExist == true
		puts "No UPVOTE button found"
	else
		touch("* text:'UPVOTE'")
	end
end

Then(/^I should see Upvoted button$/)do
	if isExist == true
	puts "No UPVOTE button found"
	else
	buttonName ="UPVOTED"
	textAfterClick = query("* marked:'textView_Wall_liked'", :text).first
	puts "Now button text is upvoted :#{(textAfterClick).eql?(buttonName)}"
	end
end

#This scenario is to find 'UPVOTE' button on available page of app and click on it
isExist1 = true
Then(/^I touch on Upvote button now$/)do
	wait_poll(:until_exists => "* text:'UPVOTE'", :timeout => 20) do
		if !element_exists("* text:'UPVOTE'")
			isExist1 = false
			break
		end
	end
	if isExist1 == true
		touch("* marked:'textView_Wall_liked'")
	else
		puts "No Upvote button is available on opened page of app"
	end

end

Then(/^I should see Upvoted button$/)do
	if isExist1==true
	buttonName ="UPVOTED"
	textAfterClick = query("* marked:'textView_Wall_liked'", :text).first
	puts "Now button text is upvoted :#{(textAfterClick).eql?(buttonName)}"
	else
	puts "No UPVOTE button found in opened page of app"
	end
end

Then(/^I touch on Comment button$/) do
	wait_poll(:until_exists => "* text:'Comments'", :timeout => 20) do
	end
	element = query("* text:'Comments'")
	touch(element)
	sleep(2)
end

Then(/^I touch on Comment Text Box$/)do
	sleep(5)
	wait_poll(:until_exists => "* marked:'editText_Comment'", :timeout => 20) do
	end
	touch ("* marked:'editText_Comment'")
end

Then(/^I write comment in comment box$/)do
	sleep(5)
	keyboard_enter_text("First Test Comment")
	
end

commentCountBefore =0
 Then(/^I should see available numbers in Comments page$/)do
	wait_poll(:until_exists => "* {text CONTAINS 'Comments'}", :timeout => 20) do
	commentBefore =  query("* {text CONTAINS 'Comments'}", :text).first
	puts "Before posting new comment total comments are: #{(commentBefore)}"
	commentCountBefore = commentBefore.gsub!(/\D/,"")
	puts "Total available comments are: #{(commentCount)}"
	end
 end

 Then(/^I press Enter button$/)do
	 #wait_poll(:until_exists => "* marked:'button_Send_Comment'", :timeout => 20) do
	 sleep(5)
	 touch("* marked:'button_Send_Comment'")
	 #end
 end 

  Then(/^I should see incrased number in Comments page$/)do
	  commentAfter = query("* {text CONTAINS 'Comments'}", :text).first 
	  puts "After posting new comment total comments are: #{(commentAfter)}"
	  commentCountAfter = commentAfter.gsub!(/\D/,"")
	  commentCountBefore = commentCountBefore + 1;  
	  puts "Total comments after adding new comment1 : #{(commentCountBefore)}"
	  puts "Total comments after adding new comment : #{(commentCountAfter)}"
	  puts "Total available comments in comment page:#{(commentCountBefore).eql?(commentCountAfter)}"
	
  end

# Then(/^I press Back button of Comment page$/)do
	# touch("ImageButton")
	
# end

#Then (/^I should see Comment button with incresed number$/)do
	
#end 



#12-11-2016

Then(/^I press Home Menu button$/) do
        element1 = query("ImageButton")
        touch(element1)
end
Then(/^I should side menu$/) do
        puts element_exists("* marked:'side_panel_user_cover_image'")
end
# Open Home Menu
Then(/^I press Discover button$/) do
        element2 = query("android.widget.TextView text:'Discover'")
		puts element_exists("* marked:'side_panel_icon'")
        touch(element2)
		
end
Then(/^I should see Discover page$/) do
        puts element_exists("* marked:'wall_main_toolbar'")
        sleep(5)
end
Then(/^I press on follow button$/) do
        element3 = query("android.widget.TextView text:'Follow'")
		touch(element3)
		puts element_exists("* marked:'discover_follow_btn'")
		end
		
		Then(/^I should see selected category as All categories$/) do
    wait_poll(:until_exists => "* text:'All Category'", :timeout => 20) do
    end
        element4 = "All Category"
        nowCategory = query("* marked:'discover_dropdown_text'", :text).first
        puts "Now selected category is: #{(nowCategory).eql? (element4)}"
end
Then(/^I press All categories dropdown$/) do
    wait_poll(:until_exists => "* marked:'discover_dropdown_icon'", :timeout => 20) do
    end
        element5 = query("android.widget.ImageView marked:'discover_dropdown_icon'")
        touch(element5)
end
Then(/^I press any category name$/) do
    wait_poll(:until_exists => "* marked:'dropdown_category_name'", :timeout => 20) do
    end
        element6 = query("android.widget.TextView text:'Software'")
        touch(element6)
end
Then(/^I should see selected category as Software$/) do
    wait_poll(:until_exists => "* text:'Software'", :timeout => 20) do
    end
        element7 = "Software"
        nowCategory = query("* marked:'discover_dropdown_text'", :text).first
        puts "Now selected category is: #{(nowCategory).eql? (element7)}"
end


Then(/^I press on notification$/) do
        element8 = query("android.widget.TextView text:'Notifications'")
		puts element_exists("* marked:'side_panel_title'")
        touch(element8)
		
end
Then(/^I should see notification page$/) do
        puts element_exists("* marked:'wall_main_toolbar'")
        sleep(5)
end

Then(/^I press on Settings$/) do
        element9 = query("android.widget.TextView text:'Settings'")
		puts element_exists("* marked:'side_panel_title'")
        touch(element9)
		
end
Then(/^I should see Settings page$/) do
        puts element_exists("* marked:'wall_main_toolbar'")
        sleep(5)
end
Then(/^I change settings$/) do
   	element10 = query("* marked:'setting_notification_switch'")
	touch(element10)
       end

Then(/^I press on Feedback$/) do
        element11 = query("android.widget.TextView text:'FeedBack'")
		puts element_exists("* marked:'side_panel_title'")
        touch(element11)
			
end
Then(/^I should see Feedback popup$/) do
        puts element_exists("* marked:'content'")
        sleep(5)
end
Then(/^I should write comment$/) do
        element15 = query("* marked:'feedback_message_edit_text'")
        touch(element15)
		puts element_exists("* marked:'feedback_message_edit_text'")
        keyboard_enter_text("Hello World")
		hide_soft_keyboard
		end	
		
Then(/^I press submit$/) do	
        element16 = query("android.widget.TextView text:'Submit'")
		#element17 = query("* marked:'feedback_submit'", :text)
		#puts element17
        touch(element16)
end
Then(/^I press on About Us$/) do
        element11 = query("android.widget.TextView text:'About Us'")
		puts element_exists("* marked:'side_panel_title'")
        touch(element11)
			
end
Then(/^I should see terms of Service page$/) do
		element7 = "Terms of Service"
		
		
		
        element19 = query("android.widget.TextView'", :text).first
		
		puts element19
		
		puts "Terms of Service is displayed: #{(element19).eql? (element7)}"
        sleep(5)
end

Then(/^I click on logout$/) do
        element12 = query("android.widget.TextView text:'Logout'")
		puts element_exists("* marked:'side_panel_title'")
        touch(element12)
			
end

Then(/^I see logout popup$/) do
puts element_exists("* marked:'content'")
        sleep(5)
		end
Then(/^I click on Yes$/) do	
        element16 = query("android.widget.TextView text:'Yes'")
		touch(element16)		
		end
Then(/^I should see sign in page$/) do
	wait_poll(:until_exists => "* marked:'button_TwitterLogin'", :timeout => 20) do
    end
	
        #element13 = query("* marked:'button_TwitterLogin'")
		puts element_exists("* marked:'button_TwitterLogin'")
		
end		
			
		
		
#12-11-2016