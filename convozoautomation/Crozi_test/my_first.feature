Feature: Crozi
@1
Scenario: Edit Name field of User Profile  
Given the app has launched
Then I press image icon
Then I should see user profile image icon 
Then I touch on user profile image icon 
Then I should see user profile page 
Then I touch on Edit button 
Then I should see user profile's details
Then I edit user name  
Then I save edited details
Then I should see edited details

@2
Scenario: Edit Introduction field of User Profile  
Given the app has launched
Then I press image icon
Then I should see user profile image icon 
Then I touch on user profile image icon 
Then I should see user profile page 
Then I touch on Edit button 
Then I should see user profile's details
Then I edit introduction
Then I save edited details
Then I should see edited details

@3
Scenario: Edit Website field of User Profile  
Given the app has launched
Then I press image icon
Then I should see user profile image icon 
Then I touch on user profile image icon 
Then I should see user profile page 
Then I touch on Edit button 
Then I should see user profile's details
Then I edit Website
Then I save edited details
Then I should see edited details

@4
Scenario: Edit Title field of User Profile  
Given the app has launched
Then I press image icon
Then I should see user profile image icon 
Then I touch on user profile image icon 
Then I should see user profile page 
Then I touch on Edit button 
Then I should see user profile's details
Then I edit Title
Then I save edited details
Then I should see edited details

@5
Scenario: Edit All Details of User Profile
Given the app has launched
Then I press image icon
Then I should see user profile image icon 
Then I touch on user profile image icon 
Then I should see user profile page 
Then I touch on Edit button 
Then I should see user profile's details
Then I edit user name 
Then I edit introduction 
Then I edit Website
Then I edit Title
Then I save edited details
Then I should see edited details

