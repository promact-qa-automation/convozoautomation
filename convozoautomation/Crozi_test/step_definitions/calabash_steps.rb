Given(/^the app has launched$/)do
#login done
end

#check edited introduction field of user profile
Then(/^I press image icon$/)do
	touch("ImageButton")
end

Then(/^I should see user profile image icon$/)do
	wait_poll(:until_exists => "* marked:'profile_image'", :timeout => 20) do
	end
	
	puts element_exists("* marked:'profile_image'")	
end

Then(/^I touch on user profile image icon$/)do
	wait_poll(:until_exists => "* marked:'profile_image'", :timeout => 20) do
	end
	
	touch("* marked:'profile_image'")
end

Then(/^I should see user profile page$/)do
	wait_poll(:until_exists =>"* marked:'action_edit_profile'", :timeout => 20) do
	end
	puts "User Profile Page is displayed"
	puts element_exists("* marked:'action_edit_profile'")	
end

Then(/^I touch on Edit button$/)do
	wait_poll(:until_exists =>"* marked:'action_edit_profile'", :timeout => 20) do
	end
	touch("* marked:'action_edit_profile'")
end

Then(/^I should see user profile's details$/)do
	wait_poll(:until_exists =>"* text:'Edit Profile'", :timeout => 20) do
	end
	puts "Edit Profile Page is displayed"
	puts element_exists("* text:'Edit Profile'")	
	
end

Then(/^I edit user name$/)do
	wait_poll(:until_exists =>"* text:'Edit Profile'", :timeout => 20) do
	end
	touch("* marked:'userNameET'")
	query("* marked:'userNameET'",{:setText=>""})
	temp= "test1"
	query("* marked:'userNameET'",{:setText=>temp})
	hide_soft_keyboard	
	scroll("ScrollView", :up)
end

#check edited introduction field of user profile
Then(/^I edit introduction$/)do
	wait_poll(:until_exists =>"* marked:'userDescriptionET'", :timeout => 20) do
		scroll("ScrollView", :down)
	end
	
	touch("* marked:'userDescriptionET'")
	query("* marked:'userDescriptionET'",{:setText=>""})
	temp= "Test Descrition"
	query("* marked:'userDescriptionET'",{:setText=>temp})
	hide_soft_keyboard
	scroll("ScrollView", :up)
end

#check edited website field of user profile
Then(/^I edit Website$/)do
	wait_poll(:until_exists =>"* marked:'userURLET'", :timeout => 20) do
		scroll("ScrollView", :down)
	end
	
	touch("* marked:'userURLET'")
	query("* marked:'userURLET'",{:setText=>""})
	temp= "Test Website"
	query("* marked:'userURLET'",{:setText=>temp})
	hide_soft_keyboard
	scroll("ScrollView", :up)
end

#check edited Title field of user profile
Then(/^I edit Title$/)do
	wait_poll(:until_exists =>"* marked:'userTypeET'", :timeout => 20) do
		scroll("ScrollView", :down)
	end
	
	touch("* marked:'userTypeET'")
	query("* marked:'userTypeET'",{:setText=>""})
	temp= "Test Title"
	query("* marked:'userTypeET'",{:setText=>temp})
	hide_soft_keyboard
	scroll("ScrollView", :up)
end

Then(/^I save edited details$/)do
	touch("* marked:'action_done_profile'")
end

Then(/^I should see edited details$/)do
	wait_poll(:until_exists =>"* marked:'message'", :timeout => 30) do
	end
	toastMessage =  query("* marked:'message'",:text).first
	puts toastMessage
	puts "profile updated: #{(toastMessage).eql? ("Profile Updated!")}"
end




